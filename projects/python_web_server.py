from bottle import route, run, template
from datetime import datetime
import sys
import Adafruit_DHT
import time
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BOARD)

pin = 4

@route('/')
def index(name='time'):
    dt = datetime.now()
    time = "{:%Y-%m-%d %H:%M:%S}".format(dt)
    humidity, temperature = Adafruit_DHT.read_retry(Adafruit_DHT.DHT11, pin)
    return template('<b>Pi thinks the date/time is: {{t}}, tepmerature is: {{tmp}}^C and humidity is: {{hum}}% </b>', t=time, tmp=int(temperature), hum=int(humidity))



run(host='localhost', port=3333)



