import smtplib
import sys
import Adafruit_DHT
import time
import RPi.GPIO as GPIO

GMAIL_USER = 'asdbim498@gmail.com'
GMAIL_PASS = '987654321asd'
SMTP_SERVER = 'smtp.gmail.com'
SMTP_PORT = 587

GPIO.setmode(GPIO.BOARD)
GPIO.setup(18,GPIO.OUT) #buzzer GPIO pin number
pin = 4 #temperature sensor GPIO pin number

def send_email(recipient, subject, text):
    smtpserver = smtplib.SMTP(SMTP_SERVER, SMTP_PORT)
    smtpserver.ehlo()
    smtpserver.starttls()
    smtpserver.ehlo
    smtpserver.login(GMAIL_USER, GMAIL_PASS)
    header = 'To:' + recipient + '\n' + 'From: ' + GMAIL_USER
    header = header + '\n' + 'Subject:' + subject + '\n'
    msg = header + '\n' + text + ' \n\n'
    smtpserver.sendmail(GMAIL_USER, recipient, msg)
    smtpserver.close()


alarm = False
try:
    while True:
        humidity, temperature = Adafruit_DHT.read_retry(Adafruit_DHT.DHT11, pin)
        
        if humidity is not None and temperature is not None:
            print 'Temp={0:2.2f}*  Humidity={1:2.2f}%'.format(temperature, humidity)
            if temperature > 27 and alarm == False:
                alarm = True
                GPIO.output(18,True)
                send_email('yusufakinalp@gmail.com', 'Sensed high temparature', 'Something went wrong. Be aware')
               
            elif temperature < 28 and alarm == True :
                GPIO.output(18,False)
                alarm = False
        else:
            print 'Failed to get reading. Try again!'

except KeyboardInterrupt: 
    GPIO.output(18,False)
    print('Terminated')
finally:        
        GPIO.cleanup()
        sys.exit(1)


